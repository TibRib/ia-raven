//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Script1.rc
//
#define IDR_MENU1                       101
#define ID_MENU_LOAD                    40001
#define IDM_MAP_LOAD                    40001
#define IDM_GAME_LOAD                   40001
#define IDM_NAVIGATION_SHOW_NAVGRAPH    40002
#define IDM_NAVIGATION_SHOW_PATH        40004
#define IDM_NAVIGATION_SMOOTH_PATHS_QUICK 40005
#define IDM_BOTS_SHOW_IDS               40006
#define IDM_BOTS_SHOW_HEALTH            40007
#define IDM_BOTS_SHOW_TARGET            40008
#define IDM_BOTS_SHOW_FOV               40009
#define IDM_BOTS_SHOW_SCORES            40010
#define IDM_BOTS_SHOW_GOAL_Q            40011
#define IDM_NAVIGATION_SHOW_INDICES     40012
#define IDM_MAP_ADDBOT                  40013
#define IDM_GAME_ADDBOT                 40013
#define IDM_MAP_REMOVEBOT               40014
#define IDM_GAME_REMOVEBOT              40014
#define IDM_NAVIGATION_SMOOTH_PATHS_PRECISE 40015
#define IDM_BOTS_SHOW_SENSED            40016
#define IDM_GAME_PAUSE                  40017

//Definitions des �quipes
#define IDM_ABOUT						41000
#define IDM_BOTS_SHOW_TEAMS				41001
#define IDM_ADD_TEAM					41002
#define IDM_BOTS_SHOW_LEADERBOARD		41003
#define IDM_ML_VS_BOTS					41004

//D�finitions Machine Learning
#define IDM_ML_ACQUIRE					42000
#define IDM_ML_TRAIN					42001
#define IDM_ML_USE						42002
#define IDM_ML_LOAD_MODEL				42003
#define IDM_ML_SAVE_MODEL				42004
#define IDM_ML_CLEAR_CSV				42005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40018
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
