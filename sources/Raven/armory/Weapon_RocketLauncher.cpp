#include "Weapon_RocketLauncher.h"
#include "../Raven_Bot.h"
#include "misc/Cgdi.h"
#include "../Raven_Game.h"
#include "../Raven_Map.h"
#include "../lua/Raven_Scriptor.h"
#include "fuzzy/FuzzyOperators.h"


//--------------------------- ctor --------------------------------------------
//-----------------------------------------------------------------------------
RocketLauncher::RocketLauncher(Raven_Bot*   owner):

                      Raven_Weapon(type_rocket_launcher,
                                   script->GetInt("RocketLauncher_DefaultRounds"),
                                   script->GetInt("RocketLauncher_MaxRoundsCarried"),
                                   script->GetDouble("RocketLauncher_FiringFreq"),
                                   script->GetDouble("RocketLauncher_IdealRange"),
                                   script->GetDouble("Rocket_MaxSpeed"),
                                   owner)
{
    //setup the vertex buffer
  const int NumWeaponVerts = 8;
  const Vector2D weapon[NumWeaponVerts] = {Vector2D(0, -3),
                                           Vector2D(6, -3),
                                           Vector2D(6, -1),
                                           Vector2D(15, -1),
                                           Vector2D(15, 1),
                                           Vector2D(6, 1),
                                           Vector2D(6, 3),
                                           Vector2D(0, 3)
                                           };
  for (int vtx=0; vtx<NumWeaponVerts; ++vtx)
  {
    m_vecWeaponVB.push_back(weapon[vtx]);
  }

  //setup the fuzzy module
  InitializeFuzzyModule();

}


//------------------------------ ShootAt --------------------------------------
//-----------------------------------------------------------------------------
inline void RocketLauncher::ShootAt(Vector2D pos)
{ 
  if (NumRoundsRemaining() > 0 && isReadyForNextShot())
  {
    //fire off a rocket!
    m_pOwner->GetWorld()->AddRocket(m_pOwner, pos);

    m_iNumRoundsLeft--;

    UpdateTimeWeaponIsNextAvailable();

    //add a trigger to the game so that the other bots can hear this shot
    //(provided they are within range)
    m_pOwner->GetWorld()->GetMap()->AddSoundTrigger(m_pOwner, script->GetDouble("RocketLauncher_SoundRange"));
  }
}

//---------------------------- Desirability -----------------------------------
//
//-----------------------------------------------------------------------------
double RocketLauncher::GetDesirability(double DistToTarget)
{
  if (m_iNumRoundsLeft == 0)
  {
    m_dLastDesirabilityScore = 0;
  }
  else
  {
    //fuzzify distance and amount of ammo
    m_FuzzyModule.Fuzzify("DistToTarget", DistToTarget);
    m_FuzzyModule.Fuzzify("AmmoStatus", (double)m_iNumRoundsLeft);

    m_dLastDesirabilityScore = m_FuzzyModule.DeFuzzify("Desirability", FuzzyModule::max_av);
  }

  return m_dLastDesirabilityScore;
}

//-------------------------  InitializeFuzzyModule ----------------------------
//
//  set up some fuzzy variables and rules
//-----------------------------------------------------------------------------
void RocketLauncher::InitializeFuzzyModule()
{
    FuzzyVariable& DistToTarget = m_FuzzyModule.CreateFLV("DistToTarget");
    FzSet& Target_InBlastRadius = DistToTarget.AddLeftShoulderSet("Target_InBlastRadius", 0, 20, 50);
    FzSet& Target_Close = DistToTarget.AddTriangularSet("Target_Close", 20, 50, 100);
    FzSet& Target_Medium = DistToTarget.AddTriangularSet("Target_Medium", 50, 100, 200);
    FzSet& Target_Far = DistToTarget.AddTriangularSet("Target_Far", 100, 200, 300);
    FzSet& Target_VeryFar = DistToTarget.AddRightShoulderSet("Target_VeryFar", 200, 300, 1000);

    FuzzyVariable& Desirability = m_FuzzyModule.CreateFLV("Desirability");
    FzSet& Undesirable = Desirability.AddLeftShoulderSet("Undesirable", 0, 20, 40);
    FzSet& SlightlyDesirable = Desirability.AddTriangularSet("SlightlyDesirable", 20, 40, 60);
    FzSet& Desirable = Desirability.AddTriangularSet("Desirable", 40, 60, 80);
    FzSet& VeryDesirable = Desirability.AddTriangularSet("VeryDesirable", 60, 80, 100);
    FzSet& ExtremelyDesirable = Desirability.AddTriangularSet("ExtremelyDesirable", 80, 100, 100);

    FuzzyVariable& AmmoStatus = m_FuzzyModule.CreateFLV("AmmoStatus");
    FzSet& Ammo_Critical = AmmoStatus.AddTriangularSet("Ammo_Critical", 0, 0, 10);
    FzSet& Ammo_Low = AmmoStatus.AddTriangularSet("Ammo_Low", 0, 10, 30);
    FzSet& Ammo_Some = AmmoStatus.AddTriangularSet("Ammo_Some", 10, 30, 50);
    FzSet& Ammo_Okay = AmmoStatus.AddTriangularSet("Ammo_Okay", 30, 50, 70);
    FzSet& Ammo_Loads = AmmoStatus.AddRightShoulderSet("Ammo_Loads", 50, 70, 100);


    // whatever the ammo, we don't want to shoot with a rocket launcher if there is a risk that we would be caught up in the blast radius
    m_FuzzyModule.AddRule(FzAND(Target_InBlastRadius, Ammo_Critical), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_InBlastRadius, Ammo_Low), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_InBlastRadius, Ammo_Some), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_InBlastRadius, Ammo_Okay), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_InBlastRadius, Ammo_Loads), Undesirable);

    // only a bit desirable if we have many ammos
    m_FuzzyModule.AddRule(FzAND(Target_Close, Ammo_Critical), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Close, Ammo_Low), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Close, Ammo_Some), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Close, Ammo_Okay), SlightlyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Close, Ammo_Loads), Desirable);

    // perfect range for rocket launcher
    m_FuzzyModule.AddRule(FzAND(Target_Medium, Ammo_Critical), VeryDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Medium, Ammo_Low), VeryDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Medium, Ammo_Some), ExtremelyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Medium, Ammo_Okay), ExtremelyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Medium, Ammo_Loads), ExtremelyDesirable);

    m_FuzzyModule.AddRule(FzAND(Target_Far, Ammo_Critical), SlightlyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Far, Ammo_Low), SlightlyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Far, Ammo_Some), Desirable);
    m_FuzzyModule.AddRule(FzAND(Target_Far, Ammo_Okay), VeryDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_Far, Ammo_Loads), VeryDesirable);

    m_FuzzyModule.AddRule(FzAND(Target_VeryFar, Ammo_Critical), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_VeryFar, Ammo_Low), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_VeryFar, Ammo_Some), Undesirable);
    m_FuzzyModule.AddRule(FzAND(Target_VeryFar, Ammo_Okay), SlightlyDesirable);
    m_FuzzyModule.AddRule(FzAND(Target_VeryFar, Ammo_Loads), Desirable);
}


//-------------------------------- Render -------------------------------------
//-----------------------------------------------------------------------------
void RocketLauncher::Render()
{
    m_vecWeaponVBTrans = WorldTransform(m_vecWeaponVB,
                                   m_pOwner->Pos(),
                                   m_pOwner->Facing(),
                                   m_pOwner->Facing().Perp(),
                                   m_pOwner->Scale());

  gdi->RedPen();

  gdi->ClosedShape(m_vecWeaponVBTrans);
}