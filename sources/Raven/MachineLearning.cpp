#include "MachineLearning.h"

#include "MLP.h"
#include "Debug/DebugConsole.h"

MachineLearning::MachineLearning()
{
	m_mlpModel = new MLP({ NbInput, NbInput, 1 }, { "sigmoid", "sigmoid"}, false);
	m_isLoadedOrTrained = false;
}

//Entrainement du r�seau selon un ensemble d'entrainement. Celui-ci n'annule pas les entrainements pr�c�dents
//Pour un meilleur entrainement, les donn�es doivent �tre m�lang�es et normalis�es
void MachineLearning::Fit(std::vector<TrainingSample> trainingSet)
{
	debug_con << "MLP training : " << "";
	for (int i = 0;i < NbEpoch;i++) {
		debug_con << "Epoch " << i << "";
		m_mlpModel->Train(trainingSet, learningRate, maxIteration, minErrCost, false);
	}
	m_isLoadedOrTrained = true;
}
//Estimation de l'output selon le r�seau et les inputs fournis
double MachineLearning::Predict(std::vector<double> xTest) const
{
	std::vector<double> output;
	m_mlpModel->GetOutput(xTest, &output);
	return output[0];
}

//Evaluation du r�seau : estimation de sa performance � pr�dire correctement les outputs de l'ensemble de test
//Pour un test objectif, le testSet et le trainingSet doivent �tre totalement distincts
void MachineLearning::Evaluate(std::vector<TrainingSample> testSet){
	int testSize = testSet.size();
	double sumSquareErr = 0;
	int NbError = 0;
	int NbOfTrue = 0;
	int NbOfFalse = 0;

	int origTrue = 0;
	int origFalse = 0;

	for (TrainingSample ts : testSet) {
		double prediction = Predict(ts.input_vector());
		double err = ts.output_vector()[0] - prediction;
		if (abs(err) > 0.5) { NbError++; }
		if (prediction > 0.5) NbOfTrue++; else NbOfFalse++;
		sumSquareErr += pow(err, 2);

		if (ts.output_vector()[0] > 0.5) { origTrue++; }
		else { origFalse++; }
	}

	double meanSquareErr = sumSquareErr / testSize;

	debug_con << "MLP Evaluation :" << "";
	debug_con << " - training set of " << testSize << " element" << "";
	debug_con << " - mean square error : " << meanSquareErr << "";
	debug_con << " - Number of error : " << NbError << "";
	debug_con << " - True : " << NbOfTrue << ", False : " << NbOfFalse <<"";
	debug_con << " - base True : " << origTrue << ", base False : " << origFalse <<"";
}

void MachineLearning::SaveModel(std::string path)
{
	m_mlpModel->SaveMLPNetwork(path);

	// DEBUG
	//debug_con << "Save model : " << "";
	//auto layer = m_mlpModel->GetLayerWeights(0);
	//for (auto node : layer)
	//	for (double w : node)
	//		debug_con << " " << w;
	//debug_con << "";
}

void MachineLearning::LoadModel(std::string path)
{
	delete m_mlpModel;
	m_mlpModel = new MLP(path);
	m_isLoadedOrTrained = true;

	// DEBUG
	//debug_con << "Load model : " << "";
	//auto layer = m_mlpModel->GetLayerWeights(0);
	//for (auto node : layer)
	//	for (double w : node)
	//		debug_con << " " << w;
	//debug_con << "";
}