#include "TeamWindow.h"
#include "Debug/DebugConsole.h"   

#include <Windows.h>
#include <iostream>
#include <CommCtrl.h>
#include "misc/Cgdi.h"

#include "Raven_Game.h"

//------------------------------------------------------------------------
//
// Name:   TeamWindow.cpp
//
// Desc:   Implementation of the team window behaviour
//
//
// Author: Thibaud Simon 2022
//------------------------------------------------------------------------


#pragma warning (disable : 4786)

//initialize static variable
#define botbox TeamWindow::BotCheckBox

HWND                     TeamWindow::m_hwnd = nullptr;
WNDPROC                  TeamWindow::m_defWndProc = nullptr;
HWND                     TeamWindow::m_textBox = nullptr;
HWND                     TeamWindow::m_colorBtn = nullptr;
HWND                     TeamWindow::m_confirmBtn = nullptr;
HWND                     TeamWindow::m_colorLabel = nullptr;
HWND                     TeamWindow::m_mlcheckbox = nullptr;
COLORREF                 TeamWindow::m_colorRef = RGB(0xF0, 0xF0, 0xF0);
Raven_Game*              TeamWindow::m_game = nullptr;
std::vector<botbox>      TeamWindow::m_checkboxes = std::vector<botbox>();
bool                     TeamWindow::m_useML = false;
bool                     TeamWindow::m_bDestroyed = false;
int                      TeamWindow::m_colorIndex = 0;
bool                     TeamWindow::m_bShowing = false;
int                      TeamWindow::m_iPosLeft;
int                      TeamWindow::m_iPosTop;

// ---- Get text
std::string GetText(HWND hwnd) {
    size_t size = SendMessage(hwnd, WM_GETTEXTLENGTH, 0, 0);
    if (!size) return "";
    auto text = std::string(size, '\0');
    SendMessage(hwnd, WM_GETTEXT, text.size() + 1, reinterpret_cast<LPARAM>(text.c_str()));
    return text;
}


//  ----- Color picker ----
LRESULT TeamWindow::OnColorClick(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    static COLORREF customColors[16] = { 
        colors[0],colors[1],colors[2],colors[3],
        colors[4],colors[5],colors[6],colors[7],
        colors[8],colors[9],colors[10],colors[11],
        colors[12],colors[13],colors[14], RGB(0x0, 0x0, 0x0) };
    CHOOSECOLOR chooseColor{ 0 };
    chooseColor.lStructSize = sizeof(chooseColor);
    chooseColor.hwndOwner = hwnd;
    chooseColor.rgbResult = m_colorRef;
    chooseColor.lpCustColors = customColors;
    chooseColor.Flags = CC_RGBINIT | CC_ANYCOLOR;

    if (ChooseColor(&chooseColor)) {
        chooseColor.rgbResult;

        int colorId = -1;
        for (int i = 0; i < 15; i++) {
            if (chooseColor.rgbResult == customColors[i]) {
                colorId = i;
                break;
            }
        }
        InvalidateRect(m_hwnd, 0, TRUE);

        if (colorId < 0) {
            MessageBox(m_hwnd, "Please select one of the custom colors on the bottom", "Instruction", 0);
            return CallWindowProc(m_defWndProc, hwnd, message, wParam, lParam);
        }

        m_colorRef = chooseColor.rgbResult;
        m_colorIndex = colorId;

        InvalidateRect(m_hwnd, 0, TRUE);
        return NULL;
    }

    return CallWindowProc(m_defWndProc, hwnd, message, wParam, lParam);
}

//-----------------------------------InfoWinProc-----------------------------
//
//-----------------------------------------------------------------------
LRESULT CALLBACK TeamWindow::TeamWindowProc(HWND hwnd,
    UINT msg,
    WPARAM wparam,
    LPARAM lparam)
{
    //these hold the dimensions of the client window area
    static int cxClient, cyClient;

    //font dimensions
    static int cxChar, cyChar, cxCaps, cyPage;

    int iVertPos;

    TEXTMETRIC  tm;
    SCROLLINFO  si;

    //get the size of the client window
    RECT rect;
    GetClientRect(hwnd, &rect);
    cxClient = rect.right;
    cyClient = rect.bottom;

    switch (msg)
    {
        case WM_CREATE:
        {
            //get the font info
            HDC hdc = GetDC(hwnd);

            ReleaseDC(hwnd, hdc);
        } break;

        case WM_CLOSE: {
            Off();
            return 0;
        } break;

        case WM_DESTROY: {
            Off();
            return 0;
        } break;

        case WM_COMMAND: {
            if (HIWORD(wparam) == BN_CLICKED) {
                //Color button clicked
                if (reinterpret_cast<HWND>(lparam) == m_colorBtn) {
                    return OnColorClick(hwnd, msg, wparam, lparam);
                }
                else if (reinterpret_cast<HWND>(lparam) == m_confirmBtn) {
                    //m_confirmBtn
                    std::string teamName = GetText(m_textBox);
                    if (teamName.empty()) {
                        MessageBox(m_hwnd, "Please enter a team name", "Instruction", MB_ICONEXCLAMATION);
                        return 0;
                    }
                    std::vector<int> botsSelectedIds;
                    for (botbox& b : m_checkboxes) {
                        if (b.checked) {
                            botsSelectedIds.push_back(b.botId);
                        }                    }
                    if (botsSelectedIds.empty()) {
                        MessageBox(m_hwnd, "Cannot create a team with no bots", "Instruction", MB_ICONEXCLAMATION);
                        return 0;
                    }
                    m_game->AddTeamToGame(teamName, m_colorIndex, botsSelectedIds, m_useML);
                    TeamWindow::Off();
                }
                else if (reinterpret_cast<HWND>(lparam) == m_mlcheckbox) {
                    if (m_game->IsUsingML() == false) {
                        MessageBoxA(hwnd, "You must load a model and tick use of machine learning if you want to use this option", "Warning", MB_OK | MB_ICONWARNING);
                    }
                    else {
                        m_useML = !m_useML;
                        SendMessage(m_mlcheckbox, BM_SETCHECK, m_useML, 0);
                    }
                }
                else {
                    for (botbox& chk : m_checkboxes) {
                        if (reinterpret_cast<HWND>(lparam) == chk.checkbox) {
                            chk.checked = !chk.checked;
                            SendMessage(chk.checkbox, BM_SETCHECK, chk.checked, 0);
                        }
                    }
                    
                }
                
            }
        } break;

        case WM_CTLCOLORSTATIC:
        {
            if (reinterpret_cast<HWND>(lparam) == m_colorLabel)
            {
                SetBkMode((HDC)wparam, TRANSPARENT);
                SetTextColor((HDC)wparam, m_colorRef);
                return (BOOL)GetSysColorBrush(COLOR_MENU);
            }
        }
        break;


        default: break;

    }//end switch

    return DefWindowProc(hwnd, msg, wparam, lparam);
}


//----------------------------- Create -----------------------------------
//
//------------------------------------------------------------------------
bool TeamWindow::Create()
{
    m_hwnd = NULL;
    m_iPosLeft = 0;
    m_iPosTop = 0;

    WNDCLASSEX wWin = { sizeof(WNDCLASSEX),
                         CS_HREDRAW | CS_VREDRAW,
                         TeamWindowProc,
                         0,
                         0,
                         GetModuleHandle(NULL),
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     "TeamWin",
                                     NULL };

    //register the window class
    if (!RegisterClassEx(&wWin))
    {
        MessageBox(NULL, "Registration of TeamWindow Failed!", "Error", 0);

        //exit the application
        return false;
    }


    constexpr int hbox = 24;
    constexpr int nbPerRow = 2;
    constexpr int wbox = 280 / nbPerRow;
    int nbBots = 20;
    int hgroup = ((nbBots / nbPerRow) + 1) * hbox + 30;


      // Create the info window
    
    m_hwnd = CreateWindow("TeamWin",
        "Team Creation",
        WS_OVERLAPPEDWINDOW | WS_SYSMENU | WS_SIZEBOX,
        0,
        0,
        TEAM_WINDOW_WIDTH,
        max(TEAM_WINDOW_HEIGHT, 200+hgroup),
        NULL,
        NULL,
        wWin.hInstance,
        NULL);
        
    //make sure the window creation has gone OK
    if (!m_hwnd)
    {
        MessageBox(m_hwnd, "CreateWindowEx Failed!", "Error!", 0);

        return false;
    }

    //Team name
    HWND label_name = CreateWindowEx(0, WC_STATIC, "Insert the new team name", WS_CHILD | WS_VISIBLE, 10, 10, 300, 20, m_hwnd, nullptr, nullptr, nullptr);
    m_textBox = CreateWindowEx(WS_EX_CLIENTEDGE, WC_EDIT, "", WS_CHILD | WS_VISIBLE, 10, 35, 100, 25, m_hwnd, nullptr, nullptr, nullptr);
    
    //Team color
    HWND label_col = CreateWindowEx(0, WC_STATIC, "Pick a team color:", WS_CHILD | WS_VISIBLE, 10, 70, 190, 20, m_hwnd, nullptr, nullptr, nullptr);
    m_colorBtn = CreateWindowEx(0, WC_BUTTON, "Select color...", WS_CHILD | WS_VISIBLE, 200, 67, 100, 25, m_hwnd, nullptr, nullptr, nullptr);
   
    HWND groupBox = CreateWindowEx(0, WC_BUTTON,  "Select bots", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 100, 300, hgroup, m_hwnd, nullptr, nullptr, nullptr);

    m_mlcheckbox = CreateWindowEx(0, WC_BUTTON, "Use Machine Learning?",
        WS_CHILD | BS_CHECKBOX | WS_VISIBLE,
        10,
        110 + hgroup,
        180, 25, m_hwnd, nullptr, nullptr, nullptr);

    m_confirmBtn = CreateWindowEx(0, WC_BUTTON, "Confirm", WS_CHILD | WS_VISIBLE, 200, 110+hgroup, 100, 25, m_hwnd, nullptr, nullptr, nullptr);

    m_colorLabel = CreateWindowEx(0, WC_STATIC, "[Team Color]", WS_CHILD | WS_VISIBLE, 200, 40, 100, 20, m_hwnd, nullptr, nullptr, nullptr);

    m_defWndProc = reinterpret_cast<WNDPROC>(SetWindowLongPtr(m_hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(TeamWindowProc)));

    // Show the window
    ShowWindow(m_hwnd, m_bShowing);
    //UpdateWindow(m_hwnd);
    return true;

}

// Singleton instance provider
TeamWindow::TeamWindow(Raven_Game* game)
{
    m_game = game;
    m_hwnd = nullptr;
    m_defWndProc = nullptr;
    m_textBox = nullptr;
    m_colorBtn = nullptr;
    m_confirmBtn = nullptr;
    m_colorLabel = nullptr;

    m_mlcheckbox = nullptr;
    m_useML = false;

    m_colorRef = RGB(0,0,0);
    m_colorIndex = 0;
    m_bDestroyed = false;
    m_bShowing = false;

    Create();
}

void TeamWindow::Off() {
    m_bShowing = false; ShowWindow(m_hwnd, m_bShowing);
    for (auto chk : m_checkboxes) {
        DestroyWindow(chk.checkbox);
    }
    m_checkboxes.clear();
    if (m_game != nullptr) {
        if (m_game->Paused()) {
            m_game->TogglePause();
        }
    }
}

void TeamWindow::On() {

    //Bots listing
    auto bots = m_game->GetAllBots();

    m_checkboxes.clear();

    constexpr int hbox = 24;
    constexpr int nbPerRow = 2;
    constexpr int wbox = 280 / nbPerRow;
    int nbBots = bots.size();
    int hgroup = ((nbBots / nbPerRow) + 1) * hbox + 30;

    /* For each bot, create a checkbox and register it */
    std::list<Raven_Bot*>::iterator it = bots.begin();
    for (int i = 0; i < nbBots; i++) {
        const auto bot = *it;
        std::string botname = std::string("") + "bot" + std::to_string(bot->ID());
        if (bot->HasTeam()) {
            botname += " (" + bot->GetTeam()->GetName() + ")";
        }
        HWND checkbox = CreateWindowEx(0, WC_BUTTON, botname.c_str(),
            WS_CHILD | BS_CHECKBOX | WS_VISIBLE,
            20 + (i % nbPerRow) * wbox,
            100 + (i / nbPerRow + 1) * hbox,
            wbox - 10, hbox, m_hwnd, nullptr, nullptr, nullptr);

        m_checkboxes.push_back({ checkbox,  bot->ID() });

        it++;
    }

    for (botbox b : m_checkboxes) {
        SendMessage(b.checkbox, BM_SETCHECK, BST_UNCHECKED, 0);
    }

    m_bShowing = true; ShowWindow(m_hwnd, m_bShowing);
}
