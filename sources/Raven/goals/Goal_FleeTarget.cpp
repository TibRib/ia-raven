#include "Goal_FleeTarget.h"
#include "Goal_Explore.h"
#include "Goal_MoveToPosition.h"
#include "..\Raven_Bot.h"
#include "..\Raven_SteeringBehaviors.h"



#include "debug/DebugConsole.h"
#include "misc/cgdi.h"

//---------------------------- Initialize -------------------------------------
//-----------------------------------------------------------------------------  
void Goal_FleeTarget::Activate()
{
    m_iStatus = active;

    //if this goal is reactivated then there may be some existing subgoals that
    //must be removed

    //TODO: � garder ou non ? Quand on fuit on peut faire autre chose comme attaquer les autres
    RemoveAllSubgoals();

    //it is possible for the target to die whilst this goal is active so we
    //must test to make sure the bot always has an active target
    if (m_pOwner->GetTargetSys()->isTargetPresent())
    {
        //grab a local copy of the last recorded position (LRP) of the target
        const Vector2D lrp = m_pOwner->GetTargetSys()->GetLastRecordedPosition();

        m_fleeingTo = ComputeFurthestPosition(lrp);

        AddSubgoal(new Goal_MoveToPosition(m_pOwner, m_fleeingTo));
    }

    //if their is no active target then this goal can be removed from the queue
    else
    {
        m_iStatus = completed;
    }

}

//------------------------------ Process --------------------------------------
//-----------------------------------------------------------------------------
int Goal_FleeTarget::Process()
{
    //if status is inactive, call Activate()
    ActivateIfInactive();

    if (Vec2DDistanceSq(m_pOwner->Pos(), m_fleeingTo) < 1.0)
    {
        //debug_con << m_pOwner->ID() << "----" << Vec2DDistanceSq(m_pOwner->Pos(), m_fleeingTo) << "";
        RemoveAllSubgoals();
        Terminate();
        return completed;
    }

    m_iStatus = ProcessSubgoals();

    //if target is in view this goal is NOT satisfied
    if (m_pOwner->GetTargetSys()->isTargetWithinFOV())
    {
        m_iStatus = active;
    }

    return m_iStatus;
}

Vector2D Goal_FleeTarget::ComputeFurthestPosition(const Vector2D lrp) const
{
    Vector2D ra = Vector2D(RandInRange(-0.2, 0.2) * lrp.x, RandInRange(-0.2, 0.2)* lrp.y);
    int a = m_pOwner->GetPathPlanner()->GetFurthestNodeToPosition(lrp + ra);
    return m_pOwner->GetPathPlanner()->GetNodePosition(a);
}

//---------------------------- Terminate --------------------------------------
//-----------------------------------------------------------------------------
void Goal_FleeTarget::Terminate()
{
    m_iStatus = completed;
}


//------------------------------- Render --------------------------------------
//-----------------------------------------------------------------------------
void Goal_FleeTarget::Render()
{
    //#define SHOW_LAST_RECORDED_POSITION
#ifdef SHOW_LAST_RECORDED_POSITION
  //render last recorded position as a green circle
    if (m_pOwner->GetTargetSys()->isTargetPresent())
    {
        gdi->GreenPen();
        gdi->RedBrush();
        gdi->Circle(m_pOwner->GetTargetSys()->GetLastRecordedPosition(), 3);
    }
#endif

    //forward the request to the subgoals
    Goal_Composite<Raven_Bot>::Render();

}