#ifndef GOAL_FLEE_TARGET_H
#define GOAL_FLEE_TARGET_H
#pragma warning (disable:4786)
//-----------------------------------------------------------------------------
//
//  Name:   Goal_HuntTarget.h
//
//  Author: Mat Buckland (www.ai-junkie.com)
//
//  Desc:   Causes a bot to search for its current target. Exits when target
//          is in view
//-----------------------------------------------------------------------------
#include "Goals/Goal_Composite.h"
#include "Raven_Goal_Types.h"
#include "../Raven_Bot.h"
#include "../navigation/Raven_PathPlanner.h"


class Goal_FleeTarget : public Goal_Composite<Raven_Bot>
{
private:

    //this value is set to true if the last visible position of the target
    //bot has been searched without success
    bool  m_bLVPTried;
    Vector2D m_fleeingTo;
    Vector2D ComputeFurthestPosition(const Vector2D lrp) const;

public:

    Goal_FleeTarget(Raven_Bot* pBot) :Goal_Composite<Raven_Bot>(pBot, goal_flee_target),
        m_bLVPTried(false), m_fleeingTo(Vector2D(-1000000, -1000000))
    {}

    //the usual suspects
    void Activate();
    int  Process();
    void Terminate();

    void Render();


};





#endif
