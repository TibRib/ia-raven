#pragma once

#include <vector>

#include "MLP.h"

class MachineLearning
{
	MLP* m_mlpModel = nullptr;

public:
	static constexpr unsigned int NbInput = 6;
	static constexpr double learningRate = 0.005;
	static constexpr unsigned int NbEpoch = 3;
	static constexpr unsigned int maxIteration = 5000;
	static constexpr double minErrCost = 0.0005;

	bool m_isLoadedOrTrained = false;

	MachineLearning();
	void Fit(std::vector<TrainingSample> trainingSet);
	double Predict(std::vector<double> xTest) const;
	void Evaluate(std::vector<TrainingSample> testSet);

	void SaveModel(std::string path);
	void LoadModel(std::string path);

	inline bool IsLoadedOrTrained() const { return m_isLoadedOrTrained;  }
};

