#pragma once
#include<iostream>
#include<fstream> 
#include <string>
#include <vector>
#include <algorithm>
#include <random>

#include "MLP.h"
#include "2D/Geometry.h"
#include "Debug/DebugConsole.h"

class DataProcessor {

private:
    std::vector<std::vector<double>> Data = { {} }; //ensemble de ligne de donn�es conserv� avant d'�tre enregistr� dans un fichier
    std::vector<double> lineOfData = {};            //la derni�re ligne de donn�es fourni (input et output)
    unsigned int lineBatchSize = 200;               //taille des paquets de donn�es envoy�s en m�moire
    std::string filePath;

public:
    DataProcessor(std::string PathToFile) : filePath(PathToFile) {}
    ~DataProcessor() {}

    //Fonctions d'ajout de donn�es, les �l�ments sont tous transform� en double et normalis� selon les valeur min et max
    void AddDouble(double Dvalue, double min, double max) {
        double valueBreadth = max - min;
        double normalizedValue = (valueBreadth != 0) ? (Dvalue - min) / valueBreadth : (Dvalue - min);
        Clamp(normalizedValue, 0, 1);
        lineOfData.push_back(normalizedValue);
    }
    void AddInt(int Ivalue, double min, double max) {
        double Dvalue = (double)Ivalue;
        double valueBreadth = max - min;
        double normalizedValue = (valueBreadth != 0) ? (Dvalue - min) / valueBreadth : (Dvalue - min);
        Clamp(normalizedValue, 0, 1);
        lineOfData.push_back(normalizedValue);
    }
    void AddBool(bool b) {
        int bvalue = b ? 1 : 0;
        lineOfData.push_back(bvalue);
    }
    void AddVector2D(Vector2D Vvalue, double min, double max) {
        double DvalueX = (double)Vvalue.x;
        double DvalueY = (double)Vvalue.y;
        double valueBreadth = max - min;
        double normalizedValueX = (valueBreadth != 0) ? (DvalueX - min) / valueBreadth : (DvalueX - min);
        double normalizedValueY = (valueBreadth != 0) ? (DvalueY - min) / valueBreadth : (DvalueY - min);
        Clamp(normalizedValueX, 0, 1);
        Clamp(normalizedValueY, 0, 1);

        lineOfData.push_back(normalizedValueX);
        lineOfData.push_back(normalizedValueY);
    }

    //Terminaison de la ligne de donn�es
    void SaveLine() {
        for (double d : lineOfData) {
            Data.back().push_back(d);
        }
        lineOfData.clear();

        if (Data.size() >= lineBatchSize) {
            //enregistrements dans un fichier � chaque "lineBatchSize" lignes, cela limite le nombre d'�criture dans le fichier pour am�liorer les performances
            std::fstream fout;
            fout.open(filePath, std::ios::app);
            
            for (std::vector<double> line : Data) {
                for (unsigned int i = 0;i < line.size();i++) {
                    if (i != 0) {
                        fout << ",";
                    }
                    fout << std::to_string(line[i]);
                }
                fout << "\n";
            }
            fout.close();
            Data.clear();
        }
        Data.push_back({});
	    //debug_con << "Acquiring line " << Data.size() << " of data" << "";
    }

    std::vector<double> GetLine() {
        return lineOfData;
    }

    void DiscardLine() {
        lineOfData.clear();
    }

    void GetTrainingSet(std::vector<TrainingSample>& trainingSet, std::vector<TrainingSample>& testSet, double testProportion) {
        std::vector<double> input;
        std::vector<double> output;
        std::vector<TrainingSample> set;
        trainingSet.clear();
        testSet.clear();


        std::fstream fin;
        fin.open(filePath, std::ios::in);

        std::string sline;

        //r�cup�ration des donn�es du fichier
        while (!fin.eof()) {
            getline(fin, sline);
            std::stringstream streamData(sline);
            std::string val;
            if (sline.size() > 4) { //emp�che le traitement de la derni�re ligne du fichier (constitu�e que d'un retour � la ligne)
                input.clear();
                output.clear();
                while (getline(streamData, val, ',')) {
                    double dval = std::atof(val.c_str());                    
                    input.push_back(dval);
                }
                output.push_back(input.back());
                input.pop_back();
                set.push_back(TrainingSample(input, output));
            }
        }
        fin.close();


        //shuffle set
        auto rng = std::default_random_engine{};
        std::shuffle(std::begin(set), std::end(set), rng);


        // r�cup�rer un nombre semblable de situations tir et non tir
        int one = 0;
        for (TrainingSample s : set)
        {
            if (s.output_vector()[0] > 0.5f)
            {
                trainingSet.push_back(s);
                one++;
            }
        }
        int zero = 2*one;
        int zerocount = 0;
        for (TrainingSample s : set)
        {
            if (s.output_vector()[0] <= 0.5f && zerocount < zero)
            {
                trainingSet.push_back(s);
                zerocount++;
            }
        }

        //shuffle trainingSet
        rng = std::default_random_engine{};
        std::shuffle(std::begin(trainingSet), std::end(trainingSet), rng);

        // d�finir un ensemble de test ind�pendant de l'ensemble d'entrainement
        size_t testNbr = trainingSet.size() * (size_t)testProportion;
        for (size_t i = 0; i < testNbr; i++)
        {
            testSet.push_back(trainingSet.back());
            trainingSet.pop_back();
        }
    }

    void DataProcessor::Clear()
    {
        std::remove(filePath.c_str());
        Data = { {} };
        lineOfData = {};
    }
};