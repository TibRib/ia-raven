#ifndef RAVEN_TEAM_H
#define RAVEN_TEAM_H

#include <windows.h>
#include "misc/Cgdi.h"

class Raven_Bot;

class Raven_Team {
private:
	std::string m_name;
	int m_color; //the color of the team
	Raven_Bot* m_teamTarget; //target, can be nullptr
	Vector2D m_basePosition; //Where the weapons are set 
	bool m_bBaseSet;
	unsigned int m_hashCode; //unique identifier obtained from the name

	int m_score;

	bool m_useMLForBots;

	//Base vertices for display
	std::vector<Vector2D> m_vertsTeamBase;

public:
	Raven_Team(std::string name, int color, bool useML=false);
	
	inline void UseColor(Cgdi * const gdiptr) const {
		gdiptr->SetPenColor(m_color);
	}
	inline void UseTextColor(Cgdi* const gdiptr) const {
		gdiptr->TextColor(m_color);
	}

	std::string GetName() const { return m_name; }
	Raven_Bot* GetTarget() { return m_teamTarget;  }
	unsigned int Hash() { return m_hashCode;  }

	Vector2D& GetBasePosition() { return m_basePosition;  }

	void SetTarget(Raven_Bot* teamTarget);
	void ClearTarget();

	void SetBasePosition(Vector2D pos);
	bool BaseIsSet() const { return m_bBaseSet; }

	//Operator == overloading for comparison with another team
	bool operator==(const Raven_Team& other) const;

	// score 
	int		GetScore()		{ return m_score; }
	void	AddScore()		{ m_score += 1; }
	void	ResetScore()	{ m_score = 0; }

	bool UsesML()const { return m_useMLForBots;  }
	
	void Render(); 
};

#endif