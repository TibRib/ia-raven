#include "Raven_Team.h"
#include "Raven_Bot.h"
#include <functional>

Raven_Team::Raven_Team(std::string name, int color ,bool useML) :
	m_name(name),
	m_color(color),
	m_basePosition(Vector2D(0,0)),
	m_teamTarget(nullptr),
	m_bBaseSet(false),
	m_useMLForBots(useML)
{ 
	//Obtain a unique integer identifier from the team name with a hash (C++11)
	std::hash<std::string> hasher;
	m_hashCode = hasher(name);

	m_score = 0;

	//Register vertices
	//create the vertex buffer for the graves
	const int NumripVerts = 4;
	const int size = 20;
	const Vector2D verts[NumripVerts] = { 
		Vector2D(-size, -size), Vector2D(size, -size),
		Vector2D(size, size), Vector2D(-size, size) 
	};
	for (int i = 0; i < NumripVerts; ++i)
	{
		m_vertsTeamBase.push_back(verts[i]);
	}
}

//Operator == overloading for comparison with another team
bool Raven_Team::operator==(const Raven_Team& other) const {
	return other.m_hashCode == this->m_hashCode;
}

void Raven_Team::SetTarget(Raven_Bot* target) {
	m_teamTarget = target;
	m_teamTarget->SetIsTargettedBy(this);
}

void Raven_Team::ClearTarget() {
	if (m_teamTarget == nullptr) return;

	m_teamTarget->SetIsTargettedBy(nullptr);
	m_teamTarget = nullptr;
}

void Raven_Team::SetBasePosition(Vector2D pos)
{ 
	m_bBaseSet = true; 
	m_basePosition = pos;
}


//Render the team drop zone (base)
void Raven_Team::Render()
{
	if (m_bBaseSet == false)
		return;

	Vector2D facing(-1, 0);

	std::vector<Vector2D> worldVerts = WorldTransform(m_vertsTeamBase, 
		m_basePosition, facing, facing.Perp(), Vector2D(1, 1));

	UseColor(gdi);
	gdi->ClosedShape(worldVerts);

	//UseTextColor(gdi);
	//gdi->TextAtPos(m_basePosition.x - 10, m_basePosition.y - 5, m_name.c_str());
}
