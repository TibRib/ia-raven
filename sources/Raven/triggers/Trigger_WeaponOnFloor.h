#ifndef WEAPON_FLOOR_H
#define WEAPON_FLOOR_H
#pragma warning (disable:4786)

#include "Triggers/Trigger.h"
#include "../Raven_Bot.h"
#include <iosfwd>

// Duplicate class of weapon giver trigger, this time not repeating

class Trigger_WeaponOnFloor : public Trigger<Raven_Bot>
{
private:

	//vrtex buffers for rocket shape
	std::vector<Vector2D>         m_vecRLVB;
	std::vector<Vector2D>         m_vecRLVBTrans;

public:
	//Addition : support a dynamic trigger instanciation
	Trigger_WeaponOnFloor(unsigned int entityType, int BRadius, int nodeIndex, Vector2D pos);

	//if triggered, this trigger will call the PickupWeapon method of the
	//bot. PickupWeapon will instantiate a weapon of the appropriate type.
	void Try(Raven_Bot*);
	void Update() {}

	//draws a symbol representing the weapon type at the trigger's location
	void Render();
};




#endif