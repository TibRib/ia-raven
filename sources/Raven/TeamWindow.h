#ifndef TEAM_WINDOW_H
#define TEAM_WINDOW_H
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
// Name:   TeamWindow.h
//
// Desc:   Creates a window interface for creating a new team.
//
//
// Author: Thibaud Simon 2022
//------------------------------------------------------------------------

#include <vector>
#include <windows.h>
#include <iosfwd>
#include <fstream>
#include <vector>

#include "misc/utils.h"
#include "misc/WindowUtils.h"

class Raven_Game;
class Raven_Bot;

//initial dimensions of the window
const int TEAM_WINDOW_WIDTH = 340;
const int TEAM_WINDOW_HEIGHT = 200;

class TeamWindow
{
private:

    static HWND	         m_hwnd;
    static WNDPROC       m_defWndProc;
    static HWND          m_textBox;
    static HWND          m_colorBtn;
    static HWND          m_confirmBtn;
    static HWND          m_colorLabel;

    static HWND          m_mlcheckbox;
    //Checkbox validation for ML usage in the team
    static bool          m_useML;

    static COLORREF      m_colorRef;
    static int           m_colorIndex;
    
    static Raven_Game*   m_game;

    struct BotCheckBox {
        HWND checkbox;
        int botId;
        bool checked=false;
    };

    static std::vector<BotCheckBox> m_checkboxes;

    //position of debug window
    static int           m_iPosTop;
    static int           m_iPosLeft;

    //set to true if the window is destroyed
    static bool          m_bDestroyed;

    //if false the window will be hidden
    static bool          m_bShowing;


    //the debug window message handler
    static LRESULT CALLBACK TeamWindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

    //this registers the window class and creates the window(called by the ctor)
    bool             Create();

    void             DrawWindow() { InvalidateRect(m_hwnd, NULL, TRUE); UpdateWindow(m_hwnd); }

    static LRESULT OnColorClick(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

public:
    TeamWindow(Raven_Game* game);
    ~TeamWindow() {
        DestroyWindow(m_hwnd); 
        m_bDestroyed = true;
    }

    //use to activate deactivate
    
    static void  Off();
    static void  On();

    bool Destroyed()const { return m_bDestroyed; }
   
};



#endif